/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parallelMerge;

import java.util.Arrays;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

/**
 *
 * @author gmaal
 */
public class ParallelMerger {

    public ParallelMerger() {
    }

    public int[] merge(int[] a, int[] b) {
        ForkJoinPool f = new ForkJoinPool();
        Merger m = new Merger(a, b);
        int[] c = f.invoke(m);
        return c;
    }

    class Merger extends RecursiveTask<int[]> {

        private int[] a, b;

        public Merger(int[] a, int[] b) {
            this.a = a;
            this.b = b;
        }

        @Override
        protected int[] compute() {

            // a empty -> return b, nothing to do here
            if (a.length == 0) {
                return b;
            }

            // get indices of middle element in a and position in b where it will be inserted 
            int amid = Math.floorDiv(a.length - 1, 2);
            int apos = findPos(a[amid]);

            // split a and b into a1, a2, b1, b2
            int[] a1 = Arrays.copyOfRange(a, 0, amid);
            int[] a2 = Arrays.copyOfRange(a, amid + 1, a.length);
            int[] b1 = Arrays.copyOfRange(b, 0, apos + 1);
            int[] b2 = Arrays.copyOfRange(b, apos, b.length);

            // insert middle element of a in last field of b1
            b1[apos] = a[amid];

            // b1/b2 empty -> return b2/b1, no more recursions necessary
            if (b1.length == 0) {
                return b2;
            }
            if (b2.length == 0) {
                return b1;
            }

            Merger m1 = new Merger(a1, b1);
            Merger m2 = new Merger(a2, b2);
            m1.fork();
            m2.fork();

            // forks return arrays s1 and s2 on join
            int[] s1 = m1.join();
            int[] s2 = m2.join();

            // combine s1 and s2 in s; both arrays are sorted, s1 contains only smaller numbers than s2
            int[] s = new int[s1.length + s2.length];
            int si = 0;
            for (int i = 0; i < s1.length; i++) {
                s[si] = s1[i];
                si++;
            }
            for (int i = 0; i < s2.length; i++) {
                s[si] = s2[i];
                si++;
            }

            return s;
        }

        private int findPos(int x) {
            int pos = Arrays.binarySearch(b, x);
            if (pos < 0) {
                pos = pos * (-1) - 1;
            }
            return pos;
        }

    }

}
