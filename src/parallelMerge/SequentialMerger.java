/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parallelMerge;

/**
 *
 * @author gmaal
 */
public class SequentialMerger {

    public SequentialMerger() {
    }

    public int[] merge(int[] a, int[] b) {
        int[] s = new int[a.length + b.length];
        int i = 0;
        int j = 0;
        int k = 0;
        while (i < a.length && j < b.length) {
            if (a[i] <= b[j]) {
                s[k] = a[i];
                i++;
                k++;
            } else {
                s[k] = b[j];
                j++;
                k++;
            }
        }
        if (i < a.length) {
            b = a;
            j = i;
        }
        while (k < s.length) {
            s[k] = b[j];
            k++;
            j++;
        }
        return s;
    }

}
