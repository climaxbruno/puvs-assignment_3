/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parallelMerge;

import java.util.Random;

/**
 *
 * @author gmaal
 */
public class Main {

    public static void main(String[] args) {
        int[] a = fillSortedArray(100);
        System.out.println("A");
        System.out.print("|");
        for (int i : a) {
            System.out.print(i + "|");
        }
        System.out.println("");

        int[] b = fillSortedArray(100);
        System.out.println("B");
        System.out.print("|");
        for (int i : b) {
            System.out.print(i + "|");
        }
        System.out.println("");

        ParallelMerger pm = new ParallelMerger();
        int[] sorted = pm.merge(a, b);

//        SequentialMerger sm = new SequentialMerger();
//        int[] sorted = sm.merge(a, b);

        System.out.println("A+B");
        System.out.print("|");
        for (int i = 0; i < sorted.length; i++) {
            System.out.print(sorted[i] + "|");
        }
    }

    private static int[] fillSortedArray(int size) {
        int[] arr = new int[size];
        int rnd = 0;
        for (int i = 0; i < size; i++) {
            rnd += new Random().nextInt(10);
            arr[i] = rnd;
        }
        return arr;
    }
}
